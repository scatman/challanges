#!/usr/bin/env python3

from bs4 import BeautifulSoup
import requests
import base64
from PIL import Image
import pytesseract
import cv2
import subprocess


URL = "http://challenge01.root-me.org/programmation/ch8/"
filename = "test.png"
# <img src="data:image/png;base64,iVBOR...">
# <form action="" method="POST"><input type="text" name="cametu"><input type="submit" value="Try"></form>


r = requests.get(URL)
soup = BeautifulSoup(r.content)
i = soup.img.attrs
src = i['src']
srcs = src.split(",")
pic_raw = srcs[-1]
print(pic_raw)
with open("test.png", "wb") as pic:
    pic.write(base64.b64decode(pic_raw))

# https://stackoverflow.com/questions/48279667/scrapy-simple-captcha-solving-example
# image = cv2.imread('test.png')
# gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
# gray = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
# gray = cv2.medianBlur(gray, 3)
# cv2.imwrite('test2.png', gray)

# conv = ["convert", "test.png", "-deskew","40%","-colorspace", "gray",
#        "-threshold", "10%", "test2.png"]

# https://www.imagemagick.org/discourse-server/viewtopic.php?t=18707
conv = ["convert", "test.png", "-morphology", "thicken", "3x1:1,0,1",
        "test2.png"]
subprocess.run(conv)

postdata = pytesseract.image_to_string(Image.open('test2.png'))
custom_oem_psm_config = r'--oem 3 --psm 7'  # psm 8 = single word
#postdata = pytesseract.image_to_string(Image.open('test.png'),config=custom_oem_psm_config)

hauweg = [" ", ",", ".", ":", ";","-","_"]
postdata = postdata.strip()
for i in hauweg:
    postdata = postdata.replace(i,"")


print(postdata)
myobj = {'cametu':postdata} 
print(myobj)
answ = requests.post(URL, data = myobj)
print(answ.text)

