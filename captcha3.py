#!/usr/bin/env python3

from selenium import webdriver
import base64
from PIL import Image
import pytesseract
import subprocess


URL = "http://challenge01.root-me.org/programmation/ch8/"
# <img src="data:image/png;base64,iVBOR...">
# <form action="" method="POST"><input type="text" name="cametu"><input type="submit" value="Try"></form>

options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/usr/bin/chromium"
driver = webdriver.Chrome(chrome_options=options)

driver.get(URL)

# https://pythonspot.com/selenium-get-images/
image = driver.find_element_by_tag_name('img')
src = image.get_attribute('src')
srcs = src.split(",")
pic_raw = srcs[-1]
print(pic_raw)
with open("test.png", "wb") as pic:
    pic.write(base64.b64decode(pic_raw))


# https://www.imagemagick.org/discourse-server/viewtopic.php?t=18707
conv = ["convert", "test.png", "-morphology", "thicken", "3x1:1,0,1",
        "test2.png"]
subprocess.run(conv)

postdata = pytesseract.image_to_string(Image.open('test2.png'))

hauweg = [" ", ",", ".", ":", ";","-","_"]
postdata = postdata.strip()
for i in hauweg:
    postdata = postdata.replace(i,"")
print(postdata)

# https://www.guru99.com/accessing-forms-in-webdriver.html 
field = driver.find_element_by_name("cametu")
field.send_keys(postdata)
field.submit()
# driver.close()
