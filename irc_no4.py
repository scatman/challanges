#!/usr/bin/env python3

# https://www.root-me.org/de/Challenges/Programmierung/IRC-The-Roman-s-wheel

import socket
from threading import Thread
import math
import base64
import codecs
import zlib


HOST = "irc.root-me.org"
PORT = 6667
CHANNEL = "#root-me_challenge"
BOT = "candy"
USER = "Hans"


def sendmsgs(s, msg=None):
    if msg:
        msg = msg + "\r\n"
        s.send(msg.encode())
        return
    else:
        while True:
            msg = input("msg: ")
            msg = msg.strip()
            msg = msg + "\r\n"
            s.send(msg.encode())

def recvmsg(s):
    msg = ""
    while True:
        try:
            msg = s.recv(1024)
        except socket.timeout:
            pass
        else:
            if len(msg) != 0:
                msg = msg.decode('utf-8')
                msg = msg.strip('\n')
                print(msg)
                # msg: recv:  :Candy!Candy@root-me.org PRIVMSG Hans :911 / 8326
                if str("PRIVMSG {}".format(USER)) in msg:
                    i = msg.rfind(":")
                    print(msg[i+1::])
                    al = (msg[i+1::]).split("/")
                    # sendmsgs(s, ansmsg(al[0], al[1]))
                    # sendmsgs(s, ansmsg2(al[0]))
                    sendmsgs(s, ansmsg4(al[0]))


def ansmsg4(a):
    b = base64.b64decode(a)
    c = zlib.decompress(b)
    c = c.decode('UTF-8')
    d = "PRIVMSG {} !ep4 -rep {}".format(BOT, c)
    print(d)
    return d 

def ansmsg2(a):
    b = base64.b64decode(a)
    b = b.decode('UTF-8')
    c = "PRIVMSG {} !ep2 -rep {}".format(BOT, b)
    print(c)
    return c


def ansmsg3(a):
    # ROT 13
    b = codecs.decode(a, 'rot_13')
    c = "PRIVMSG {} !ep3 -rep {}".format(BOT, b)
    print(c)
    return c


def ansmsg(a, b):
    '''
    You must calculate the square root of the number n°1 
    - and multiply the result by the number n°2.
    - Then you need to round to two decimals.
    '''
    a = int(a)
    b = int(b)
    x = math.sqrt(a)
    y = x * b
    z = "PRIVMSG {} !ep1 -rep {:.2f}".format(BOT, y)
    return z


def main():

    # connect
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(1)  # timeout for send and receive
    s.connect((HOST, PORT))

    # thread
    t2 = Thread(target=recvmsg, args=(s,))
    t2.start()

    sendmsgs(s, "NICK "+USER)
    sendmsgs(s, "USER {} * * :{}".format(USER, USER))
    sendmsgs(s, "JOIN "+CHANNEL)
    t1 = Thread(target=sendmsgs, args=(s,))
    t1.start()


if __name__ == "__main__":
    main()
